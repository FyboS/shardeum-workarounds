# Introdution
This is a temporary workaround that needs to be used after each restart of the container.

# Problem
- During the installation of the node, an error appears: *Error Loading extension section v3 req* and *cat: selfsigned_node.crt / selfisned.key: No such file or directory*
- In pm2 log: *Error: ENOENT: no such file or directory, open '/home/node/app/gui/selfsigned.key*

# Fix

Connect to your server and run the following commands (*not in container*):
1. `curl -O https://gitlab.com/FyboS/shardeum-workarounds/-/raw/main/selfsigned.key/selfsignedkey.sh`
2. `docker exec -w /home/node/app/gui -i shardeum-dashboard bash < selfsignedkey.sh`
