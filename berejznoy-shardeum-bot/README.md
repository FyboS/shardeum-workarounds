# Introdution
This is a bash script that can be used to install the bot..

# Instruction

1. `curl -O https://gitlab.com/FyboS/shardeum-workarounds/-/raw/main/berejznoy-shardeum-bot/berejznoy-shardeum-bot-installer.sh`
2. `chmod +x berejznoy-shardeum-bot-installer.sh`
3. `./berejznoy-shardeum-bot-installer.sh`
